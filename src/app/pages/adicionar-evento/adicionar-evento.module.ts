import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { AdicionarEventoPageRoutingModule } from './adicionar-evento-routing.module';

import { AdicionarEventoPage } from './adicionar-evento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([{ path: '', component: AdicionarEventoPage }]),
    AdicionarEventoPageRoutingModule
  ],
  declarations: [AdicionarEventoPage]
})
export class AdicionarEventoPageModule {}
