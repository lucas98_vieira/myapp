import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdicionarEventoPage } from './adicionar-evento.page';

const routes: Routes = [
  {
    path: '',
    component: AdicionarEventoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdicionarEventoPageRoutingModule {}
