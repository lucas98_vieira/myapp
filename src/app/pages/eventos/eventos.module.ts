import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EventosPageRoutingModule } from './eventos-routing.module';

import { RouterModule } from '@angular/router';

import { EventosPage } from './eventos.page';
import {AdicionarEventoPage} from '../adicionar-evento/adicionar-evento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EventosPageRoutingModule,
    RouterModule.forChild([{ path: '', component: AdicionarEventoPage }]),
  ],
  declarations: [EventosPage]
})
export class EventosPageModule {}
